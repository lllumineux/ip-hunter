from flask import url_for


def test_add_alias_get_request(login_client):
    res = login_client.get(url_for('aliases.add_alias'))
    assert res.status_code == 200
    assert b'Add alias' in res.data


def test_add_alias_post_request(login_client):
    res = login_client.post(
        url_for('aliases.add_alias'),
        data={
            'ip_address': '127.0.0.1',
            'alias': 'me'
        }
    )
    assert res.status_code == 200


def test_edit_alias_get_request(login_client_with_alias):
    res = login_client_with_alias.get(url_for('aliases.edit_alias', alias_id=1))
    assert res.status_code == 200
    assert b'Edit alias' in res.data


def test_edit_alias_post_request(login_client_with_alias):
    res = login_client_with_alias.post(
        url_for('aliases.edit_alias', alias_id=1),
        data={
            'alias': 'me again'
        }
    )
    assert res.status_code == 200


def test_aliases(login_client):
    res = login_client.get(url_for('aliases.aliases'))
    assert res.status_code == 200
    assert b'My aliases' in res.data


def test_delete_alias(login_client_with_alias):
    res = login_client_with_alias.get(url_for('aliases.delete_alias', alias_id=1))
    assert res.status_code == 302
