from flask import url_for


def test_signup_get_request(unauthenticated_client):
    res = unauthenticated_client.get(url_for('accounts.signup'))
    assert res.status_code == 200
    assert b'Confirm password' in res.data


def test_signup_post_request(unauthenticated_client):
    res = unauthenticated_client.post(
        url_for('accounts.signup'),
        data={
            'username': 'client_2',
            'password': 'qwerty!12345',
            'confirm_password': 'qwerty!12345'
        }
    )
    assert res.status_code == 200


def test_login_get_request(unauthenticated_client):
    res = unauthenticated_client.get(url_for('accounts.login'))
    assert res.status_code == 200
    assert b'Remember me' in res.data


def test_login_post_request(signup_client):
    res = signup_client.post(
        url_for('accounts.login'),
        data={
            'username': 'client_1',
            'password': '12345!qwerty'
        }
    )
    assert res.status_code == 302


def test_logout(login_client):
    res = login_client.get(url_for('accounts.logout'))
    assert res.status_code == 302


def test_profile_get_request(login_client):
    res = login_client.get(url_for('accounts.profile'))
    assert res.status_code == 200
    assert b'Edit profile' in res.data


def test_profile_post_request(login_client):
    res = login_client.post(
        url_for('accounts.profile'),
        data={
            'current_password': '12345!qwerty',
            'new_username': 'client_1_renamed'
        }
    )
    assert res.status_code == 200
