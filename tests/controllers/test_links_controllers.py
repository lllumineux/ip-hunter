import json

from flask import url_for


def test_add_link_get_request(login_client):
    res = login_client.get(url_for('links.add_link'))
    assert res.status_code == 200
    assert b'Add link' in res.data


def test_add_link_post_request(login_client):
    res = login_client.post(
        url_for('links.add_link'),
        data={
            'target_link': 'https://youtu.be/dQw4w9WgXcQ',
            'label': 'Try press the link :)'
        }
    )
    assert res.status_code == 200


def test_link_get_request(login_client_with_link):
    res = login_client_with_link.get(url_for('links.link', link_id=1))
    assert res.status_code == 200
    assert b'Link' in res.data


def test_link_post_request(login_client_with_link):
    res = login_client_with_link.post(url_for('links.link', link_id=1))
    assert res.status_code == 200
    assert json.loads(res.data) == {'new_visits': []}


def test_links(login_client):
    res = login_client.get(url_for('links.links'))
    assert res.status_code == 200
    assert b'My links' in res.data


def test_add_visit(login_client_with_link):
    res = login_client_with_link.get(url_for('links.add_visit', link_id=1))
    assert res.status_code == 302


def test_delete_link(login_client_with_link):
    res = login_client_with_link.get(url_for('links.delete_link', link_id=1))
    assert res.status_code == 302
