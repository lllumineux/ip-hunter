from app.links.services import generate_short_link, get_location_by_ip


def test_generate_short_links():
    generated_link = generate_short_link('https://youtu.be/dQw4w9WgXcQ')
    assert type(generated_link) is str


def test_get_location_by_ip():
    location = get_location_by_ip('178.204.251.30')
    assert type(location) is str
