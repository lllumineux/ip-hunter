import pytest
from flask import url_for
from werkzeug.security import generate_password_hash

from app import create_app, db
from app.accounts.models import User
from app.aliases.models import Alias
from app.configs import TestConfig
from app.links.models import Link


@pytest.fixture
def app():
    app = create_app()
    app.config.from_object(TestConfig)

    with app.app_context():
        db.create_all()

    yield app

    with app.app_context():
        db.session.remove()
        db.drop_all()


@pytest.fixture
def unauthenticated_client(client):
    yield client


@pytest.fixture
def signup_client(client):
    # Create user
    user = User(username='client_1', hashed_password=generate_password_hash('12345!qwerty'))
    db.session.add(user)
    db.session.commit()

    yield client


@pytest.fixture
def login_client(client):
    # Create user
    user = User(username='client_1', hashed_password=generate_password_hash('12345!qwerty'))
    db.session.add(user)
    db.session.commit()
    # Login user
    client.post(url_for('accounts.login'), data={'username': 'client_1', 'password': '12345!qwerty'})

    yield client

    client.get(url_for('accounts.logout'))


@pytest.fixture
def login_client_with_alias(client):
    # Create user
    user = User(username='client_1', hashed_password=generate_password_hash('12345!qwerty'))
    db.session.add(user)
    db.session.commit()
    # Login user
    client.post(url_for('accounts.login'), data={'username': 'client_1', 'password': '12345!qwerty'})
    # Create alias
    alias = Alias(ip_address='127.0.0.1', alias='me', user_id=user.id)
    db.session.add(alias)
    db.session.commit()

    yield client

    client.get(url_for('accounts.logout'))


@pytest.fixture
def login_client_with_link(client):
    # Create user
    user = User(username='client_1', hashed_password=generate_password_hash('12345!qwerty'))
    db.session.add(user)
    db.session.commit()
    # Login user
    client.post(url_for('accounts.login'), data={'username': 'client_1', 'password': '12345!qwerty'})
    # Create link
    link = Link(redirect_link='', target_link='https://youtu.be/dQw4w9WgXcQ', label='Try press the link :)', user_id=user.id)
    db.session.add(link)
    db.session.commit()

    yield client

    client.get(url_for('accounts.logout'))
