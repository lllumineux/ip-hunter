def prettify_datetime(datetime_obj):
    return datetime_obj.strftime("%m/%d/%Y, %H:%M:%S")
