from app.aliases.controllers import aliases_bp
from app.accounts.controllers import accounts_bp
from app.links.controllers import links_bp
from app.utils.filters import prettify_datetime


def register_all_blueprints(app):
    app.register_blueprint(accounts_bp, url_prefix='/')
    app.register_blueprint(links_bp, url_prefix='/')
    app.register_blueprint(aliases_bp, url_prefix='/')


def register_all_filters(app):
    app.template_filter()(prettify_datetime)
