from flask import request
from sqlalchemy import desc

from app.accounts.models import User
from app.db import db
from app.links.models import Link, Visit
from app.links.services import generate_short_link, get_location_by_ip


def create_link(target_link: str, label: str, user: User) -> None:
    new_link = Link(redirect_link='', target_link=target_link, label=label or None, user_id=user.id)
    db.session.add(new_link)
    db.session.commit()
    new_link.redirect_link = generate_short_link(f'{request.url_root}add_visit/{new_link.id}')
    db.session.commit()


def get_links_by_user_id(user_id: str or int) -> list:
    return Link.query.filter_by(user_id=int(user_id)).all()


def get_link_by_id(link_id: str or int) -> Link:
    return Link.query.filter_by(id=int(link_id)).first()


def destroy_link_by_id(link_id: str or int) -> None:
    link = Link.query.filter_by(id=int(link_id)).first()
    db.session.delete(link)
    db.session.commit()


def create_visit(ip_address: str, link: str or int) -> None:
    new_visit = Visit(ip_address=ip_address, location=get_location_by_ip(ip_address), link_id=link.id)
    db.session.add(new_visit)
    db.session.commit()


def get_visits_by_link_id(link_id: str or int) -> list:
    return Visit.query.filter_by(link_id=int(link_id)).order_by(desc(Visit.time_visited)).all()
