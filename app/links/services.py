import datetime
import json

import requests
from flask_login import current_user

from app.aliases.db_services import get_alias_by_ip_and_user_id
from app.constants import KUTTIT_API_TOKEN, LINK_PAGE_POLL_INTERVAL
from app.utils.filters import prettify_datetime


def generate_short_link(link: str) -> str:
    headers = {'X-API-KEY': KUTTIT_API_TOKEN, 'Content-Type': 'application/json'}
    data = {'target': link}
    response = requests.post('https://kutt.it/api/v2/links', headers=headers, data=json.dumps(data))
    response_data = json.loads(response.text)
    try:
        return response_data['link']
    except KeyError:
        return 'Link generation failed'


def get_location_by_ip(ip: str) -> str:
    params = {'lang': 'en'}
    response = requests.get(f'http://ip-api.com/json/{ip}', params=params)
    response_data = json.loads(response.text)
    if response_data['status'] == 'fail':
        return 'Location failed'
    return f'{response_data["city"]}, {response_data["regionName"]}, {response_data["country"]}'


def get_visits_with_label(visits: list) -> list:
    for visit in visits:
        alias = get_alias_by_ip_and_user_id(visit.ip_address, current_user.id)
        visit.label = alias.alias if alias else visit.ip_address
    return visits


def serialize_new_visits(visits: list) -> list:
    visits_serialized = []
    for visit in visits:
        if (datetime.datetime.utcnow() - visit.time_visited).seconds * 1000 < LINK_PAGE_POLL_INTERVAL:
            visits_serialized.append(
                {
                    'id': visit.id,
                    'label': visit.label,
                    'location': visit.location,
                    'time_visited': prettify_datetime(visit.time_visited)
                }
            )
    return visits_serialized[::-1]


def get_ip_from_request(request):
    fwd = request.environ.get('HTTP_X_FORWARDED_FOR', None)
    if fwd is None:
        return request.environ.get('REMOTE_ADDR')
    fwd = fwd.split(',')[0]
    return fwd
