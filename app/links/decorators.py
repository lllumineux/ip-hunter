from functools import wraps
from flask import abort

from flask_login import current_user

from app.links.db_services import get_link_by_id


def link_owner(f):
    @wraps(f)
    def decorated_function(link_id, *args, **kwargs):
        link = get_link_by_id(link_id)
        if not link:
            abort(404)
        if link.user_id != current_user.id:
            abort(401)
        return f(link_id, *args, **kwargs)
    return decorated_function
