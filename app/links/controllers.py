from flask import Blueprint, render_template, request, flash, redirect, url_for, abort
from flask_login import login_required, current_user

from app.links.db_services import (
    create_link, get_links_by_user_id, get_link_by_id, create_visit,
    get_visits_by_link_id, destroy_link_by_id,
)
from app.links.decorators import link_owner
from app.links.forms import AddLinkForm
from app.links.services import get_visits_with_label, serialize_new_visits, get_ip_from_request

links_bp = Blueprint('links', __name__, template_folder='templates')


@links_bp.route('/add_link', methods=('GET', 'POST'))
@login_required
def add_link():
    form = AddLinkForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            create_link(form.target_link.data, form.label.data, current_user)
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
            return redirect(url_for('links.add_link'))
        else:
            return redirect(url_for('links.links'))

    return render_template('links/add_link.html', username=current_user.username, form=form)


@links_bp.route('/link/<link_id>', methods=('GET', 'POST'))
@login_required
@link_owner
def link(link_id):
    try:
        if request.method == 'POST':
            return {'new_visits': serialize_new_visits(get_visits_with_label(get_visits_by_link_id(link_id)))}
        data = {
            'username': current_user.username,
            'link': get_link_by_id(link_id),
            'visits': get_visits_with_label(get_visits_by_link_id(link_id))
        }
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return render_template('links/link.html', **data)


@links_bp.route('/links', methods=('GET',))
@login_required
def links():
    try:
        data = {
            'username': current_user.username,
            'links': get_links_by_user_id(current_user.id)
        }
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return render_template('links/links.html', **data)


@links_bp.route('/add_visit/<link_id>', methods=('GET',))
def add_visit(link_id):
    try:
        link_by_id = get_link_by_id(link_id)
        create_visit(get_ip_from_request(request), link_by_id)
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return redirect(link_by_id.target_link)


@links_bp.route('/delete_link/<link_id>', methods=('GET',))
@login_required
@link_owner
def delete_link(link_id):
    try:
        destroy_link_by_id(link_id)
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return redirect(url_for('links.links'))
