from flask_wtf import FlaskForm
from wtforms import StringField, validators


class AddLinkForm(FlaskForm):
    target_link = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(max=256),
            validators.Regexp(regex=r'^https?:\/\/\S*$', message='Invalid link')
        ]
    )
    label = StringField(
        validators=[
            validators.Length(max=64)
        ]
    )
