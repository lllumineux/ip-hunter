import datetime

from sqlalchemy.orm import relationship

from app.accounts.models import User
from app.db import db


class Link(db.Model):
    __tablename__ = 'links'

    id = db.Column(db.Integer, primary_key=True)
    redirect_link = db.Column(db.String(256), nullable=False)
    target_link = db.Column(db.String(256), nullable=False)
    label = db.Column(db.String(64), nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id, ondelete='CASCADE'), nullable=False)

    user = relationship('User', foreign_keys='Link.user_id')

    def __repr__(self):
        return f'<Link {self.id}>'


class Visit(db.Model):
    __tablename__ = 'visits'

    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String(32), nullable=False)
    location = db.Column(db.String(256), nullable=False)
    time_visited = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    link_id = db.Column(db.Integer, db.ForeignKey(Link.id, ondelete='CASCADE'), nullable=False)

    link = relationship('Link', foreign_keys='Visit.link_id')

    def __repr__(self):
        return f'<Visit {self.id}>'
