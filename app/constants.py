import os

DATABASE_URI = os.getenv('DATABASE_URI', '')
KUTTIT_API_TOKEN = os.getenv('KUTTIT_API_TOKEN', '')
LINK_PAGE_POLL_INTERVAL = 5000  # in ms
