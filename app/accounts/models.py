from flask_login import UserMixin

from app.db import db


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(32), unique=True, index=True, nullable=False)
    hashed_password = db.Column(db.String(256), unique=False, nullable=False)

    def __repr__(self):
        return f'<User {self.id}>'
