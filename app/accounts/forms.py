from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, validators, BooleanField

from app.accounts import validators as custom_validators


class SignupForm(FlaskForm):
    username = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=4, max=32),
            validators.Regexp(regex=r'^[\w_]*$', message='Username should only contain letters, numbers, underscores'),
            custom_validators.username_taken_check
        ]
    )
    password = PasswordField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=8, max=32),
            validators.EqualTo(fieldname='confirm_password', message='Passwords must match')
        ]
    )
    confirm_password = PasswordField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=8, max=32)
        ]
    )


class LoginForm(FlaskForm):
    username = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=4, max=32)
        ]
    )
    password = PasswordField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=8, max=32)
        ]
    )
    remember_me = BooleanField()


class EditProfileForm(FlaskForm):
    current_password = PasswordField(
        validators=[
            validators.DataRequired(),
            validators.Length(min=8, max=32),
            custom_validators.current_user_password_match_check
        ]
    )
    new_username = StringField(
        validators=[
            validators.Length(min=4, max=32),
            validators.Regexp(regex=r'^[\w_]*$', message='Username should only contain letters, numbers, underscores'),
            custom_validators.username_taken_check
        ]
    )
    new_password = PasswordField(
        validators=[
            validators.EqualTo(fieldname='confirm_new_password', message='Passwords must match')
        ]
    )
    confirm_new_password = PasswordField()
