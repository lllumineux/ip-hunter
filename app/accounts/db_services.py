from sqlalchemy import text
from werkzeug.security import generate_password_hash, check_password_hash

from app.accounts.models import User
from app.db import db


def create_user(username: str, password: str) -> None:
    new_user = User(username=username, hashed_password=generate_password_hash(password))
    db.session.add(new_user)
    db.session.commit()


# Raw SQL request as needed in technical requirements
def get_user_dict_by_name(username: str) -> dict:
    sql = text(f'select * from users where username = \'{username}\'')
    sql_res = db.engine.execute(sql).first()
    if not sql_res:
        return {}
    return {'id': sql_res[0], 'username': sql_res[1], 'hashed_password': sql_res[2]}


def get_user_by_name_and_password(username: str, password: str) -> User or None:
    user = User.query.filter_by(username=username).first()
    if user and check_password_hash(user.hashed_password, password):
        return user


def get_user_by_id(user_id: str or int) -> User:
    return User.query.filter_by(id=int(user_id)).first()


def update_user(user: User, new_username=None, new_password=None) -> None:
    if new_username:
        user.username = new_username
    if new_password:
        user.hashed_password = generate_password_hash(new_password)
    db.session.commit()
