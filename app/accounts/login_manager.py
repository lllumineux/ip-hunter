from flask_login import LoginManager

from app.accounts.db_services import get_user_by_id


login_manager = LoginManager()


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return get_user_by_id(user_id)
    return None
