from flask import Blueprint, redirect, url_for, render_template, request, flash
from flask_login import login_user, login_required, logout_user, current_user
from wtforms import ValidationError

from app.accounts.db_services import create_user, get_user_by_name_and_password, update_user
from app.accounts.forms import SignupForm, LoginForm, EditProfileForm

accounts_bp = Blueprint('accounts', __name__, template_folder='templates')


@accounts_bp.route('/', methods=('GET',))
def index():
    if current_user.is_authenticated:
        return redirect(url_for('links.links'))
    return redirect(url_for('accounts.login'))


@accounts_bp.route('/signup', methods=('GET', 'POST'))
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('links.links'))

    form = SignupForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            create_user(form.username.data, form.password.data)
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
            return redirect(url_for('accounts.signup'))
        else:
            return redirect(url_for('accounts.login'))

    return render_template('auth/signup.html', form=form)


@accounts_bp.route('/login', methods=('GET', 'POST'))
def login():
    if current_user.is_authenticated:
        return redirect(url_for('links.links'))

    form = LoginForm(request.form)

    if request.method == 'POST':
        try:
            user = get_user_by_name_and_password(form.username.data, form.password.data)
            if not user:
                raise ValidationError('Incorrect credentials')
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
            return redirect(url_for('accounts.login'))
        else:
            login_user(user, remember=form.remember_me.data)
            return redirect(url_for('links.links'))

    return render_template('auth/login.html', form=form)


@accounts_bp.route('/logout', methods=('GET',))
def logout():
    logout_user()
    return redirect(url_for('accounts.login'))


@accounts_bp.route('/profile', methods=('GET', 'POST'))
@login_required
def profile():
    form = EditProfileForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            update_user(current_user, new_username=form.new_username.data, new_password=form.new_password.data)
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
        return redirect(url_for('accounts.profile'))

    return render_template('profile/edit_profile.html', username=current_user.username, form=form)
