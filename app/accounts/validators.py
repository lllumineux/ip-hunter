from flask_login import current_user
from werkzeug.security import check_password_hash
from wtforms import ValidationError

from app.accounts.db_services import get_user_dict_by_name


def username_taken_check(form, field):
    if get_user_dict_by_name(field.data):
        raise ValidationError('Username is already taken')


def current_user_password_match_check(form, field):
    if not check_password_hash(current_user.hashed_password, field.data):
        raise ValidationError('Current password is incorrect')
