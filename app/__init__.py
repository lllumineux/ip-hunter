import os

from dotenv import load_dotenv
from flask import Flask

from app.accounts.login_manager import login_manager
from app.configs import DevelopmentConfig
from app.db import db, migrate

from app.utils.register import register_all_blueprints, register_all_filters


def create_app():
    app = Flask(__name__)

    register_all_blueprints(app)
    register_all_filters(app)

    load_dotenv()
    if not os.getenv('IS_PRODUCTION', False):
        app.config.from_object(DevelopmentConfig)

    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)

    return app
