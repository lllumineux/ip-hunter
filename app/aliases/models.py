from sqlalchemy.orm import relationship

from app.accounts.models import User
from app.db import db


class Alias(db.Model):
    __tablename__ = 'aliases'

    id = db.Column(db.Integer, primary_key=True)
    ip_address = db.Column(db.String(32), nullable=False)
    alias = db.Column(db.String(64), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey(User.id, ondelete='CASCADE'), nullable=False)

    user = relationship('User', foreign_keys='Alias.user_id')

    def __repr__(self):
        return f'<Alias {self.id}>'
