from functools import wraps
from flask import abort

from flask_login import current_user

from app.aliases.db_services import get_alias_by_id


def alias_owner(f):
    @wraps(f)
    def decorated_function(alias_id, *args, **kwargs):
        alias = get_alias_by_id(alias_id)
        if not alias:
            abort(404)
        if alias.user_id != current_user.id:
            abort(401)
        return f(alias_id, *args, **kwargs)
    return decorated_function
