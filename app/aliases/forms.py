from flask_wtf import FlaskForm
from wtforms import StringField, validators


class AddAliasForm(FlaskForm):
    ip_address = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(max=19),
            validators.Regexp(regex=r'^(\d{1,3}\.){3}\d{1,3}$', message='Invalid IP')
        ]
    )
    alias = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(max=64)
        ]
    )


class EditAliasForm(FlaskForm):
    alias = StringField(
        validators=[
            validators.DataRequired(),
            validators.Length(max=64)
        ]
    )
