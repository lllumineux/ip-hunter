from app.accounts.models import User
from app.aliases.models import Alias
from app.db import db


def create_alias(ip_address: str, alias: str, user: User) -> None:
    new_alias = Alias(ip_address=ip_address, alias=alias, user_id=user.id)
    db.session.add(new_alias)
    db.session.commit()


def get_aliases_by_user_id(user_id: str or int) -> list:
    return Alias.query.filter_by(user_id=int(user_id)).all()


def get_alias_by_id(alias_id: str or int) -> Alias:
    return Alias.query.filter_by(id=int(alias_id)).first()


def get_alias_by_ip_and_user_id(ip_address: str, user_id: str or int) -> Alias:
    return Alias.query.filter_by(ip_address=ip_address, user_id=int(user_id)).first()


def update_alias(alias_id: str, alias_alias: str) -> None:
    alias = Alias.query.filter_by(id=int(alias_id)).first()
    alias.alias = alias_alias
    db.session.commit()


def destroy_alias_by_id(alias_id: str or int) -> None:
    alias = Alias.query.filter_by(id=int(alias_id)).first()
    db.session.delete(alias)
    db.session.commit()
