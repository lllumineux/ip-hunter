from flask import Blueprint, render_template, request, flash, redirect, url_for, abort
from flask_login import login_required, current_user

from app.aliases.db_services import create_alias, get_aliases_by_user_id, destroy_alias_by_id, get_alias_by_id, update_alias
from app.aliases.decorators import alias_owner
from app.aliases.forms import AddAliasForm, EditAliasForm

aliases_bp = Blueprint('aliases', __name__, template_folder='templates')


@aliases_bp.route('/add_alias', methods=('GET', 'POST'))
@login_required
def add_alias():
    form = AddAliasForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            create_alias(form.ip_address.data, form.alias.data, current_user)
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
            return redirect(url_for('aliases.add_alias'))
        else:
            return redirect(url_for('aliases.aliases'))

    return render_template('aliases/add_alias.html', username=current_user.username, form=form)


@aliases_bp.route('/edit_alias/<alias_id>', methods=('GET', 'POST'))
@login_required
@alias_owner
def edit_alias(alias_id):
    form = EditAliasForm(request.form)

    if request.method == 'POST' and form.validate():
        try:
            update_alias(alias_id, form.alias.data)
        except Exception as e:
            flash(str(e) or 'Unknown error', 'error')
            return redirect(url_for('aliases.edit_alias'))
        else:
            return redirect(url_for('aliases.aliases'))

    try:
        data = {
            'username': current_user.username,
            'alias': get_alias_by_id(alias_id),
            'form': form
        }
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return render_template('aliases/edit_alias.html', **data)


@aliases_bp.route('/aliases', methods=('GET',))
@login_required
def aliases():
    try:
        data = {
            'username': current_user.username,
            'aliases': get_aliases_by_user_id(current_user.id)
        }
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return render_template('aliases/aliases.html', **data)


@aliases_bp.route('/delete_alias/<alias_id>', methods=('GET',))
@login_required
@alias_owner
def delete_alias(alias_id):
    try:
        destroy_alias_by_id(alias_id)
    except Exception as e:
        flash(str(e) or 'Unknown error', 'error')
        abort(404)
    else:
        return redirect(url_for('aliases.aliases'))
