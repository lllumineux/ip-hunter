from app.constants import DATABASE_URI


class DevelopmentConfig:
    DEBUG = True
    SECRET_KEY = 'https://youtu.be/dQw4w9WgXcQ'
    SQLALCHEMY_DATABASE_URI = DATABASE_URI
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = True


class TestConfig:
    SECRET_KEY = 'https://youtu.be/dQw4w9WgXcQ'
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
