"""Add links table

Revision ID: 1b3b3650ed19
Revises: 7ebf3ac27c27
Create Date: 2021-10-28 00:07:51.330898

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1b3b3650ed19'
down_revision = '7ebf3ac27c27'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'links',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('redirect_link', sa.String(length=256), nullable=False),
        sa.Column('target_link', sa.String(length=256), nullable=False),
        sa.Column('label', sa.String(length=64), nullable=True),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('links')
