"""Add visits table

Revision ID: cea6e056085c
Revises: 1b3b3650ed19
Create Date: 2021-10-28 00:10:40.836466

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'cea6e056085c'
down_revision = '1b3b3650ed19'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'visits',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('ip_address', sa.String(length=32), nullable=False),
        sa.Column('location', sa.String(length=256), nullable=False),
        sa.Column('time_visited', sa.DateTime(), nullable=True),
        sa.Column('link_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['link_id'], ['links.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('visits')
