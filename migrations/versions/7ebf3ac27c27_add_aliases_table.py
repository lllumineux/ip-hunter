"""Add aliases table

Revision ID: 7ebf3ac27c27
Revises: 288d343c0fbc
Create Date: 2021-10-27 23:33:12.075600

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7ebf3ac27c27'
down_revision = '288d343c0fbc'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'aliases',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('ip_address', sa.String(length=32), nullable=False),
        sa.Column('alias', sa.String(length=64), nullable=False),
        sa.Column('user_id', sa.Integer(), nullable=False),
        sa.ForeignKeyConstraint(['user_id'], ['users.id'], ),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('aliases')
