"""Add users table

Revision ID: 288d343c0fbc
Revises: 
Create Date: 2021-10-27 22:57:50.226825

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '288d343c0fbc'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'users',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('username', sa.String(length=32), nullable=False),
        sa.Column('hashed_password', sa.String(length=256), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_users_username'), 'users', ['username'], unique=True)


def downgrade():
    op.drop_index(op.f('ix_users_username'), table_name='users')
    op.drop_table('users')
