# IP-Hunter

## What is IP-Hunter?

### Description
An application that allows authenticated users to create short links from URLs. As soon as someone clicks on the generated link, their IP and location (up to the city) become visible to the link creator. Also, users can attach aliases to the specified IP addresses for convenience.

### Overview video
[![Project overview](https://img.youtube.com/vi/pTzTCv5UM1g/0.jpg)](https://youtu.be/pTzTCv5UM1g)

## How to run?
1. set `KUTTIT_API_TOKEN` and `DATABASE_URI` environment variables
2. open project directory in cmd
3. run `poetry install`
4. run `flask db upgrade`
5. run `flask run`
